#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#define SAMPLERATE	44100	//samples per second
#define O0C		16.3516	//lowest C
//useless
//enum waveType{quiet, sine, triangle, square, saw, noise};

//song element
	//pointer to pcmdata
	//pointer to next pcmdata

typedef struct pcmData_struct{
	//string or hash that describers what note this is
	int16_t* samples;
	uint32_t length;
} pcmData;

//notes	octave
float calcNoteFreq(uint8_t note, uint8_t octave, int8_t mod){
	float inote = 0;
	float ioctave = octave;
	/*switch (note%7) {
		case 2://C
			inote = 0.0;
		case 3://D
			inote = 2.0;
		case 4://E
			inote = 4.0;
		case 5://F
			inote = 5.0;
		case 6://G
			inote = 7.0;
		case 0://A
			inote = 9.0;
		case 1://H
			inote = 11.0;
	}*/
	if(note == 2)
		inote = 0.0;
	if(note == 3)
		inote = 2.0;
	if(note == 4)
		inote = 4.0;
	if(note == 5)
		inote = 5.0;
	if(note == 6)
		inote = 7.0;
	if(note == 0)
		inote = 9.0;
	if(note == 1)
		inote = 11.0;

	inote -=(mod < 0)? 1.0 : 0.0;	//check flat
	inote +=(mod > 0)? 1.0 : 0.0;	//check shart

	printf("%d %f %d %f %d, %f\t", note, inote, octave, ioctave, mod, 16.35159783128741466737*powf(2, (inote+(12.0*ioctave))/12));
	return 16.35159783128741466737*powf(2, (inote+(12.0*ioctave))/12);
}


int getNoteSampleLength(int smxNum, int tempo){
	return SAMPLERATE*60/tempo*4/smxNum;
}

int16_t genSquare(float frequency, uint32_t samplep){
	float deltaPeriod = SAMPLERATE/frequency;		//how long does 1 take
	float subDeltaPeriod = deltaPeriod/2;
	float curenSubPeriod = fmodf(samplep, deltaPeriod);

	int16_t sample = (curenSubPeriod < subDeltaPeriod) ? INT16_MAX : INT16_MIN;
	return sample;
}

int16_t genSaw(float frequency, uint32_t samplep){
	float deltaPeriod = SAMPLERATE/frequency;
	int curenSubPeriod = fmodf(samplep, deltaPeriod);

	int minVolume = INT16_MIN;
	int maxRange = UINT16_MAX;
	int sawi = minVolume+((maxRange/deltaPeriod)*curenSubPeriod);


	int16_t sample = sawi;
	return sample;
}

pcmData* generateWave(uint32_t lenght, float frequency){
	pcmData* gWave = malloc(sizeof(pcmData));
	gWave->length = lenght;//getNoteSampleLength(lenght, tempo);
	gWave->samples = malloc(sizeof(int16_t)*lenght);

//	printf("s2c:%d\n", lenght);
	for(uint32_t samplep = 0; samplep < gWave->length; samplep++){
		int16_t sample;
		sample = genSquare(frequency, samplep);//change to function pointer
		gWave->samples[samplep]=sample;
	}
	return gWave;
}

pcmData* concatPcmDataMix(pcmData* PCM0, pcmData* PCM1, int32_t offset){
	//pcm0 is the current full track from sample0 and pcm1 is a "sample"
	uint32_t length = offset + PCM1->length;
	pcmData* cWave = malloc(sizeof(pcmData));
	cWave->samples = malloc(sizeof(int16_t)*length);
	cWave->length = length;
	for(uint32_t i = 0; i < length; i++){
		int16_t sample = 0;
		if (i < offset){
			sample = PCM0->samples[i];
		} else if(i < PCM0->length){
			int32_t mix  = (PCM0->samples[i]+PCM1->samples[i-offset])/2;
			sample = mix;
		} else {
			sample = PCM1->samples[i-offset];
		}
		cWave->samples[i]=sample;
	}
	free(PCM0->samples);
	free(PCM0);
	free(PCM1->samples);
	free(PCM1);
	return cWave;
}

pcmData* playNote(float frequency, uint8_t volume, uint32_t lenght, uint32_t tempo){
	//make our pcmData Buffer
	pcmData* note = generateWave(lenght, frequency);
	//change volume
	float mulVol = (1.0/15)*volume;
	for(uint32_t i = 0; i < note->length; i++){
		note->samples[i] *= mulVol;
		if (i > (note->length-(note->length/5))){
			float trnd = fmodf(i, (float)note->length/5);
			note->samples[i] *= mulVol - (mulVol / ((float)note->length/5) * trnd);
		}
	}
	return note;
}

int parseNumber(char* str, uint32_t* offset){
	int num=0;
	for(*offset += 1; str[*offset]>='0' && str[*offset]<='9'; *offset += 1){
		num = num*10+(str[*offset]-'0');
		num = (num==0) ? -1 : num ;
	}
	num = (num==0) ? -1 : num;
	return num;
}

uint32_t parsePlayLength(char* prog, uint32_t* offset, uint32_t defaultLength, uint8_t tempo){
	int32_t playLenght = defaultLength;
	if(prog[*offset]=='.'){
		*offset+=1;
		return playLenght*3/2;
	}
	playLenght = parseNumber(prog, offset);
	playLenght = (playLenght < 0) ? defaultLength : getNoteSampleLength(playLenght, tempo);
	if(prog[*offset+1] == '.'){
		playLenght = playLenght*3/2;
		*offset += 1;
	}
	return playLenght;
}
void parseNote(char* prog, uint32_t* offset, int8_t* noteV, int8_t* fsi, uint32_t* playLenght, uint32_t defaultLength, uint8_t tempo){
	//setup the target note number
	int8_t tragetNote = prog[*offset]-'A';
	*fsi = 0;
	*fsi = (prog[*offset+1] == '+') ?  1 : *fsi;//check if +
	*fsi = (prog[*offset+1] == '#') ?  1 : *fsi;//check if +
	*fsi = (prog[*offset+1] == '-') ? -1 : *fsi;//check if -
	//running numbers
	uint32_t tmpPlayLength=0;
	
	*offset -= 1;//have offset simulate being on a '&'
	do{
		//make sure that note is correct
		*offset += 1;//hop over a '&'
		int8_t tNote = prog[*offset]-'A';
		int8_t lfsi = 0;
		lfsi = (prog[*offset+1] == '+') ?  1 : lfsi;//check if +
		lfsi = (prog[*offset+1] == '#') ?  1 : lfsi;//check if +
		lfsi = (prog[*offset+1] == '-') ? -1 : lfsi;//check if -

		if(tNote != tragetNote || lfsi != *fsi){break;}
		*offset += ( *fsi != 0 ) ? 1 : 0;

		tmpPlayLength += parsePlayLength(prog, offset, defaultLength, tempo);

	}while(prog[*offset]=='&');
	*offset-=1;

	//decode and set note
	*noteV = tragetNote;

	//set play length
	*playLenght = (tmpPlayLength > 0) ? tmpPlayLength : defaultLength;
}

pcmData* playSMX(char* prog){
	pcmData* track = malloc(sizeof(pcmData));
	track->length = 5;
	track->samples=malloc(sizeof(int16_t)*5);

	int8_t octaveV = 4;	//4
	int8_t noteV = 2;	//C
	uint8_t tempo = 120;
	uint8_t volume = 8;
	uint32_t playLenght = getNoteSampleLength(4, tempo);	//change to the ammount of samples to play
	uint32_t defaultLength = getNoteSampleLength(4, tempo);	//same as above kinda required for '&' and higher accuracity note gen
	uint8_t dls=4;
	int8_t fsi = 0;		//flat sharp indicator -=flat 0=normal +=sharp

	//enum waveType instrm = square;
	for(uint32_t i = 0; prog[i] != '\0'; i++){
		printf("%c:%d\tV%d:\tO%d:\tN%d:\tFSI%d:\tT%d:\tL%d:\tP%d:\n", prog[i], i, volume, octaveV, noteV, fsi, tempo, defaultLength, playLenght);
		if(prog[i] >= 'A' && prog[i] <= 'G'){//add '&' handler for even longernotes "might fix the sample volume problem"
			parseNote(prog, &i, &noteV, &fsi, &playLenght, defaultLength, tempo);
			pcmData* note = playNote(calcNoteFreq(noteV, octaveV, fsi), volume, playLenght, tempo);
			track = concatPcmDataMix(track, note, track->length);
		}else if(prog[i] == '^'){
			//read next nums
			playLenght = parsePlayLength(prog, &i, defaultLength, tempo);
			//play accordingly
			pcmData* note = playNote(calcNoteFreq(noteV, octaveV, fsi), volume, playLenght, tempo);
			track = concatPcmDataMix(track, note, track->length);
		}else if(prog[i] == 'R'){
			//read nums
			playLenght = parsePlayLength(prog, &i, defaultLength, tempo);
			//make empty
			pcmData* empty = malloc(sizeof(pcmData));
			empty->length = playLenght;
			empty->samples = malloc(sizeof(int16_t)*empty->length);
			track = concatPcmDataMix(track, empty, track->length);
		}else if(prog[i] == '<' || prog[i] == '>'){
			octaveV += (prog[i]=='<') ? -1 : 1;
			octaveV = (octaveV > 8) ? 8 : octaveV;	//prevent overflow
			octaveV = (octaveV < 1) ? 1 : octaveV;	//prevent underflow
		}else if(prog[i] == 'V'){
			//read next nums set volume accordingly
			int vp = parseNumber(prog, &i)%16;
			volume = (vp < 1) ? 8 : vp ;
		}else if(prog[i] == 'T'){
			int tmp = parseNumber(prog, &i);
			tempo = (tmp < 32) ? 120 : tmp%256;
			defaultLength = getNoteSampleLength(dls, tempo);
		}else if(prog[i] == 'L'){
			int dl = parseNumber(prog, &i);
			defaultLength = (dl < 0) ? getNoteSampleLength(4, tempo) : getNoteSampleLength(dl, tempo);
			dls = (dl < 0) ? 4 : dl;
		}else if(prog[i] == 'O'){
			int oct = parseNumber(prog, &i);
			octaveV = (oct < 1) ? 1 : oct;
			octaveV = (oct > 8) ? 8 : oct;
		}
	}
	return track;
}


int main(int argc, char* argv[]){
	
	for (int i = 0; i < argc; i++){
		printf("argv[%d] = %s\n", i, argv[i]);
	}
	pcmData* track = playSMX( argv[1] );

	FILE *ptr_fp;
	if((ptr_fp = fopen("funk.rb", "wb")) == NULL){
		printf("Unable to open file!\n");
		exit(1);
	}else printf("Opened file successfully for writing.\n");

	if( fwrite(track->samples, track->length*sizeof(int16_t), 1, ptr_fp) != 1){
		printf("Write error!\n");
		exit(1);
	}else printf("Write was successful.\n");
	fclose(ptr_fp);

	free(track->samples);
	free(track);

	return 0;
}