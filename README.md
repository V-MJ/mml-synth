# Incomplete MusicMacroLanguage parser

this program takes in mml and creates a file full of raw signed 16bit samples.

# building

`sh build.sh`

# running

`./funk "mmlgoes here"`

# notes

You can open the produced file in audacity by importing `raw data` and setting the import settings to `signed 16-bit pcm`, `Little-endian`, `1 Channel` and `44100`.

As for the mml, I tried to make the parser some what compatible with the mml from the mmo Mabinogi. Just make sure that your mml doesn't have extra whitespace or lowercase letters in it. A simple mml should look like this `T120FFFAGGGBAAGGF3R.AAAA>C3<B3GGGGB3A3FFFAGGGBAAGGF3` and it can be run by typing (`./funk "T120FFFAGGGBAAGGF3R.AAAA>C3<B3GGGGB3A3FFFAGGGBAAGGF3"`) to your terminal.